const fetch = require('node-fetch');
const { xml2js, js2xml } = require('xml-js');
const { from } = require('./lib/index.js');

(async () => {
  const res = await fetch('https://files.transkribus.eu/Get?id=HWZHIERNXVLJVBPPDLFUJFYO');
  const data = from(await res.text(), { what: 'lines' });
  console.log(data);
})();

(async () => {
  const res = await fetch('https://files.transkribus.eu/Get?id=HWZHIERNXVLJVBPPDLFUJFYO');
  const data = from(await res.text(), { what: 'regions+lines+attributes' });
  console.log(data);
})();

{
  function parsePointsAttr(points) {
    return 'parsed' + points
  }

  const options = {
    compact: false,
    alwaysArray: true,
    alwaysChildren: true,
    nativeType: true,
    nativeTypeAttributes: true,
    elementsKey: 'children',
    attributeValueFn: (value, name, parent) => name === 'points' ? parsePointsAttr(value) : value
  };
  var data = xml2js('<root><x number="1" yup="a" points="something">asfd</x><y>1</y></root>', options)
  // console.log(data)
}

{
  function toPointsAttr(points) {
    return points;
  }

  const options = {
    compact: false,
    alwaysArray: true,
    alwaysChildren: true,
    nativeType: true,
    nativeTypeAttributes: true,
    elementsKey: 'children',
    attributeValueFn: (value, name, parent) => name === 'points' ? toPointsAttr(value) : value  
  };
  // console.log(js2xml(data, options))
}

(async () => {
  const res = await fetch('https://files.transkribus.eu/Get?id=HWZHIERNXVLJVBPPDLFUJFYO');
  const data = from(await res.text(), { what: 'elements' });
})();
